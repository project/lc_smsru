<?php

class lcSmsRuDelivery extends leadCaptureDelivery {

  private static function getApiKey() {
    return variable_get('lc_smsru_api_id');
  }

  private static function getPhoneNumber() {
    return variable_get('lc_smsru_phone_number');
  }

  private static function getSenderName() {
    return variable_get('lc_smsru_sender_name');
  }

  public static function run($actionHandler) {
    $apiKey = self::getApiKey();
    $phone_number = self::getPhoneNumber();
    $sender_name = self::getSenderName();
    if (!$apiKey || !$phoneNumber) {
      watchdog('LC sms.ru', 'Please configure the LC sms.ru service settings', array(), WATCHDOG_ERROR);
      return;
    }
    $context = $actionHandler->getContext();
    $data = $actionHandler->getData();
    $message = $context['title'];
    $context['phone_number'] = $phone_number;
    // Allow other modules to alter message before sending sms.
    drupal_alter('lc_smsru_message', $message, $context, $data);
    $query = array(
      'api_id' => $apiKey,
      'to' => $context['phone_number'],
      'msg' => $message,
      'json' => 1,
    );
    
    if ($sender_name) {
      $query['from'] = $sender_name;
    }
    
    $url = url('https://sms.ru/sms/send', array('query' => $query));
    $request_json = file_get_contents($url);
    $request = drupal_json_decode($request_json);
    self::checkresponse($request, $phoneNumber);
  }

  public static function checkresponse($request, $phoneNumber) {
    if (empty($request['sms'][$phoneNumber])) {
      watchdog('LC sms.ru', 'Please check LC sms.ru module configs', WATCHDOG_ERROR);
      return;
    }
    $status = $request['sms'][$phoneNumber]['status'];
    if ($status != 'OK') {
      watchdog('LC sms.ru', 'Please check LC sms.ru response: !response', array('!response' => drupal_json_encode($request)), WATCHDOG_ERROR);
    }
  }

}
